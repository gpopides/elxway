# ---- Build Stage ----
FROM elixir:1.11 as app_builder

# Set environment variables for building the application
ENV MIX_ENV=prod \
    TEST=1 \
    LANG=C.UTF-8

# Fetch the latest version of Elixir (once the 1.9 docker image is available you won't have to do this)

# Install hex and rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# Create the application build directory
RUN mkdir /app
WORKDIR /app

# Copy over all the necessary application files and directories
COPY lib ./lib
COPY mix.exs .
COPY mix.lock .

# Fetch the application dependencies and build the application
RUN mix deps.clean --all
RUN mix deps.get
RUN mix deps.compile
RUN mix release

# ---- Application Stage ----
FROM bitnami/minideb:latest AS app

ENV LANG=C.UTF-8

# Install openssl
RUN apt-get update && apt-get install -y openssl libncurses5-dev


# Copy over the build artifact from the previous step and create a non root user
RUN adduser --home /home/app --disabled-password --gecos "" app
WORKDIR /home/app
COPY --from=app_builder /app/_build .
RUN chown -R app: ./prod
USER app

# Run the app
CMD ["./prod/rel/prod/bin/prod", "start"]
