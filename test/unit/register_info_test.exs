defmodule RegisterInfoTest do
  use ExUnit.Case
  alias Elxway.Structs.RegisterInfo
  # doctest Elxway

  test "creates_new_register_info" do
    expected = %RegisterInfo{
      name: "name",
      host: "127.0.0.1:7000",
      paths: [
        "/api/documents"
      ]
    }

    assert {:ok, expected} ==
             RegisterInfo.new(%{
               body_params: %{
                 "host" => "127.0.0.1:7000",
                 "paths" => [
                   "/api/documents"
                 ],
                 "name" => "name"
               }
             })
  end

  test "creates_new_register_info fails" do
    assert {:error} == RegisterInfo.new(%{})
  end

  test "creates_new_register_info fails empty body" do
    assert {:error} == RegisterInfo.new(%{body_params: %{}})
  end
end
