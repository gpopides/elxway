defmodule RegistryManagerTest do
  use ExUnit.Case
  alias Elxway.Structs.RegisterInfo
  alias Elxway.ServiceRegistry.Manager

  test "register endpoints adds auth service to state" do
    host = "127.0.0.1:7000"

    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => host,
          "paths" => [
            "api/documents"
          ],
          "name" => "authentication-service"
        }
      })

    Manager.register_endpoints(info)

    assert host == Manager.get_host(:auth)
  end

  test "register 2 endpoints" do
    host1 = "127.0.0.1:7000"
    host2 = "127.0.0.1:8000"

    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => host1,
          "paths" => [
            "api/documents"
          ],
          "name" => "authentication-service"
        }
      })

    Manager.register_endpoints(info)

    {:ok, info2} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => host2,
          "paths" => [
            "api/otherendpoint"
          ],
          "name" => "someother-service"
        }
      })

    Manager.register_endpoints(info2)

    # wait a second for the genserver(worker) state to be updated
    Process.sleep(1)
    assert {:ok, host1} == Manager.get_host(["api", "documents"]) 
    assert {:ok, host2} == Manager.get_host(["api", "otherendpoint"])
  end
end
