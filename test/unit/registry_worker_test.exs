defmodule RegistryWorkerTest do
  use ExUnit.Case
  alias Elxway.Structs.RegisterInfo
  alias Elxway.ServiceRegistry.Worker
  alias Elxway.ServiceRegistry.Registry.Utils

  setup do
    name = :worker_1
    Worker.start_link(name: name)
    {:ok, %{worker_name: name}}
  end

  test "find host returns correct host", state do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents"
          ],
          "name" => "name"
        }
      })

    Worker.update_paths_tree(state.worker_name, Utils.create_paths_tree(info))

    assert {:ok, "127.0.0.1:7000"} == Worker.get_host(state.worker_name, ["api", "documents"])
  end

  test "find host returns not found because path does not exist", state do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents"
          ],
          "name" => "name"
        }
      })

    Worker.update_paths_tree(state.worker_name, Utils.create_paths_tree(info))

    assert {:error, "not_found", 404} == Worker.get_host(state.worker_name, ["api", "documents2"])
  end

  test "find host with entity id returns correct host", state do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents",
            "api/documents/{documentId}"
          ],
          "name" => "name"
        }
      })

    Worker.update_paths_tree(state.worker_name, Utils.create_paths_tree(info))

    assert {:ok, "127.0.0.1:7000"} ==
             Worker.get_host(state.worker_name, ["api", "documents", "1"])
  end

  test "find host with entity id returns not found invalid entity id", state do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents",
            "api/documents/{documentId}"
          ],
          "name" => "name"
        }
      })

    Worker.update_paths_tree(state.worker_name, Utils.create_paths_tree(info))

    assert {:error, "not_found", 404} ==
             Worker.get_host(state.worker_name, ["api", "documents", "invalidId"])
  end

  test "find host using UUID as id returns expected host", state do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents",
            "api/documents/{documentId}"
          ],
          "name" => "name"
        }
      })

    Worker.update_paths_tree(state.worker_name, Utils.create_paths_tree(info))

    assert {:ok, "127.0.0.1:7000"} ==
             Worker.get_host(state.worker_name, ["api", "documents", UUID.uuid4()])
  end

  test "find_host returns error because path is not found", state do
    assert {:error, "not_found", 404} == Worker.get_host(state.worker_name, ["api", "path1"])
  end

  test "find_host returns error because tree is empty", state do
    assert {:error, "invalid_path", 400} == Worker.get_host(state.worker_name, [])
  end
end
