defmodule RegistryUtilsTest do
  use ExUnit.Case
  alias Elxway.ServiceRegistry.Registry.Utils
  alias Elxway.Structs.RegisterInfo

  test "creates the paths tree with 1 level" do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents"
          ],
          "name" => "name"
        }
      })

    expected_cast_result = %{
      "api" => %{
        subpaths: %{
          "documents" => %{
            subpaths: %{},
            host: "127.0.0.1:7000"
          }
        },
        host: "127.0.0.1:7000"
      }
    }

    assert expected_cast_result == Utils.create_paths_tree(info)
  end

  test "creates the paths tree 3 levels" do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents",
            "api/documents/share",
            "api/directories"
          ],
          "name" => "name"
        }
      })

    expected_cast_result = %{
      "api" => %{
        host: "127.0.0.1:7000",
        subpaths: %{
          "documents" => %{
            host: "127.0.0.1:7000",
            subpaths: %{
              "share" => %{
                subpaths: %{},
                host: "127.0.0.1:7000"
              }
            }
          },
          "directories" => %{
            subpaths: %{},
            host: "127.0.0.1:7000"
          }
        }
      }
    }

    assert expected_cast_result == Utils.create_paths_tree(info)
  end

  test "creates the paths tree 2 levels" do
    {:ok, info} =
      RegisterInfo.new(%{
        remote_ip: {127, 0, 0, 1},
        port: 7000,
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents",
            "api/directories"
          ],
          "name" => "name"
        }
      })

    expected_cast_result = %{
      "api" => %{
        subpaths: %{
          "documents" => %{
            subpaths: %{},
            host: "127.0.0.1:7000"
          },
          "directories" => %{
            subpaths: %{},
            host: "127.0.0.1:7000"
          }
        },
        host: "127.0.0.1:7000"
      }
    }

    assert expected_cast_result == Utils.create_paths_tree(info)
  end

  test "creates the paths tree with 3 levels subpath has 2 subpaths" do
    {:ok, info} =
      RegisterInfo.new(%{
        body_params: %{
          "host" => "127.0.0.1:7000",
          "paths" => [
            "api/documents/share",
            "api/documents/test",
            "api/directories"
          ],
          "name" => "name"
        }
      })

    expected_cast_result = %{
      "api" => %{
        host: "127.0.0.1:7000",
        subpaths: %{
          "documents" => %{
            subpaths: %{
              "share" => %{
                subpaths: %{},
                host: "127.0.0.1:7000"
              },
              "test" => %{
                subpaths: %{},
                host: "127.0.0.1:7000"
              }
            },
            host: "127.0.0.1:7000"
          },
          "directories" => %{
            subpaths: %{},
            host: "127.0.0.1:7000"
          }
        }
      }
    }

    assert expected_cast_result == Utils.create_paths_tree(info)
  end
end
