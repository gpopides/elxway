defmodule RegisterInfoTest do
  use ExUnit.Case
  alias Elxway.HandlerUtils

  test "get_forward_url without query params" do
    host = "http://182.555.123.444"
    path_info = ["api", "endpoint"]
    query_params = %{}
    expected = "#{host}/" <> Enum.join(path_info, "/")
    assert expected == HandlerUtils.get_forward_url(host, path_info, query_params)
  end

  test "get_forward_url with 1 query param" do
    host = "http://182.555.123.444"
    path_info = ["api", "endpoint"]
    query_params = %{param1: "someparam"}
    expected = "#{host}/" <> Enum.join(path_info, "/") <> "?param1=someparam"
    assert expected == HandlerUtils.get_forward_url(host, path_info, query_params)
  end

  test "get_forward_url with 2 query params" do
    host = "http://182.555.123.444"
    path_info = ["api", "endpoint"]
    query_params = %{param1: "someparam", param2: "some_other_param"}

    expected =
      "#{host}/" <> Enum.join(path_info, "/") <> "?param1=someparam&param2=some_other_param"

    assert expected == HandlerUtils.get_forward_url(host, path_info, query_params)
  end

  test "is_multipart content_type returns true" do
    content_type = ["multipart/form-data; boundary=18188sss"]
    assert true == HandlerUtils.is_multipart?(content_type)
  end

  test "is_multipart content_type returns false for invalid content type" do
    content_type = ["multipart/form-dat"]
    assert false == HandlerUtils.is_multipart?(content_type)
  end

  test "is_multipart content_type returns false for json content type" do
    content_type = ["application/json"]
    assert false == HandlerUtils.is_multipart?(content_type)
  end
end
