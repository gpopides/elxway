defmodule AuthTest do
  use ExUnit.Case
  alias Elxway.Auth

  test "path info is whitelisted " do
    whitelisted_path_infos = [
      ["api", "login"]
    ]


    Enum.each(whitelisted_path_infos, fn path_info -> 
      assert true == Auth.path_info_is_whitelisted(path_info)
    end)
  end

  test "path info is not whitelisted " do
    whitelisted_path_infos = [
      ["path", "random"],
      ["path", "random2"]
    ]


    Enum.each(whitelisted_path_infos, fn path_info -> 
      assert false == Auth.path_info_is_whitelisted(path_info)
    end)
  end

end
