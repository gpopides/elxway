defmodule HandlerTest do
  use ExUnit.Case, async: true
  use Plug.Test

  test "test handle returns 404 route does not exist with whitelisted route" do
    conn = conn(:get, "/api/login")


    %Plug.Conn{resp_body: response} = Elxway.Handler.handle(conn)
    assert %{"error" => "not_found"} == Jason.decode!(response)
  end

  test "test handle returns missing token with non whitelisted route" do
    conn = conn(:get, "/someroute")


    %Plug.Conn{resp_body: response} = Elxway.Handler.handle(conn)
    assert %{"error" => "invalid token"} == Jason.decode!(response)
  end

  # TODO add mocking tests for external services
end
