defmodule Elxway do
  @moduledoc """
  Documentation for `Elxway`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Elxway.hello()
      :world

  """
  def hello do
    :world
  end
end
