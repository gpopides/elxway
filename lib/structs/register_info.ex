defmodule Elxway.Structs.RegisterInfo do
  defstruct paths: [], name: "", host: ""

  @type data :: map()
  @spec new(data) :: {:ok, %Elxway.Structs.RegisterInfo{}} | {:error}

  def new(data) when data == %{}, do: {:error}
  def new(%{body_params: body_params}) when body_params == %{}, do: {:error}

  def new(%{body_params: body}) do
    {:ok,
     %Elxway.Structs.RegisterInfo{
       paths: body["paths"],
       host: body["host"],
       name: body["name"]
     }}
  end
end
