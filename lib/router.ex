defmodule Elxway.Router do
  use Plug.Router
  alias Elxway.Handler

  plug(:match)
  plug(CORSPlug)

  plug(Plug.Parsers,
    parsers: [:json, :multipart],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  post("/register", do: Handler.handle_register(conn))

  get("/register",
    do:
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(400, Jason.encode!(%{error: "Method not supported"}))
  )

  delete("/register",
    do:
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(400, Jason.encode!(%{error: "Method not supported"}))
  )

  put("/register",
    do:
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(400, Jason.encode!(%{error: "Method not supported"}))
  )

  match(_, do: Handler.handle(conn))
end
