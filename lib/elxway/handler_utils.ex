defmodule Elxway.HandlerUtils do
  @type host :: String.t()
  @type path_info :: list(String.t())
  @type query_params :: map()
  @type content_types :: list(String.t())

  @spec get_forward_url(host, path_info, query_params) :: String.t()
  def get_forward_url(host, path_info, query_params) when query_params == %{},
    do: Enum.join([host | path_info], "/")

  def get_forward_url(host, path_info, query_params) when query_params != %{} do
    q_params =
      Enum.reduce(query_params, "", fn {key, value}, acc ->
        url_query_param_reducer({key, value}, acc)
      end)

    new_url = Enum.join([host | path_info], "/")
    new_url <> q_params
  end

  @spec get_forward_url(host, path_info) :: String.t()
  def get_forward_url(host, path_info), do: Enum.join([host | path_info], "/")

  @spec is_multipart?(content_types) :: boolean()
  def is_multipart?(content_types) do
    content_types |> Enum.any?(fn x -> String.starts_with?(x, "multipart/form-data") end)
  end

  defp url_query_param_reducer({key, value}, query_param_url_part)
       when query_param_url_part == "" do
    query_param_url_part <> "?#{key}=#{value}"
  end

  defp url_query_param_reducer({key, value}, query_param_url_part)
       when query_param_url_part != "" do
    query_param_url_part <> "&#{key}=#{value}"
  end
end
