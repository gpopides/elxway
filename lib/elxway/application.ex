defmodule Elxway.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Elxway.ServiceRegistry.Manager, [name: Elxway.ServiceRegistry.Manager]},
      {Plug.Cowboy, scheme: :http, plug: Elxway.Router, options: [port: 4001]},
      {Elxway.ServiceRegistry.WorkerSupervisor, [name: Elxway.ServiceRegistry.WorkerSupervisor]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Elxway.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
