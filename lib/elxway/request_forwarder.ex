defmodule Elxway.RequestForwarder do
  @moduledoc """
  This module contains functions that are responsible
  to forward the request to the appropriate host,
  based on the headers/request_body of the incomming request
  """
  require Logger

  @type destination_url() :: String.t()
  @type request_body() :: map()
  @type headers() :: list({String.t(), String.t()})

  @doc """
  Forwards requests based on the request_body type
  """
  @spec forward_post(destination_url(), request_body(), headers(), :json) ::
          {:ok, any()} | {:error, String.t()}
  def forward_post(destination_url, request_body, headers, :json) do
    with {:ok, encoded_request_body} <- Jason.encode(request_body) do
      HTTPoison.post(destination_url, encoded_request_body, headers)
    else
      {:error, _} ->
        Logger.error("Invalid JSON body in POST request")
        {:error, "invalid_request_body"}
    end
  end

  @spec forward_post(destination_url(), request_body(), headers(), :multipart) ::
          {:ok, any()} | {:error, String.t()}
  def forward_post(destination_url, request_body, headers, :multipart) do
    with {:ok, {key_of_file, path}} <- find_file_in_request_body(request_body) do
      form_data = construct_form_data(request_body, key_of_file, path)

      {:ok,
       HTTPoison.post!(
         destination_url,
         {:multipart, form_data},
         headers
       )}
    else
      {:error, _} ->
        Logger.error("Tried to forward multipart request without a file")
        {:error, "file not found in form data"}
    end
  end

  @spec forward_options(url :: destination_url()) :: any()
  def forward_options(url) do
    {:ok, HTTPoison.options!(url)}
  end

  @spec find_file_in_request_body(request_body()) ::
          {:ok, {String.t(), String.t()}} | {:error, String.t()}
  defp find_file_in_request_body(request_body) do
    is_file_upload_struct = fn _x = %Plug.Upload{} -> true end

    found_item =
      request_body
      |> Enum.find(fn {_key, value} ->
        is_file_upload_struct.(value)
      end)

    with {key, %Plug.Upload{path: path}} <- found_item do
      {:ok, {key, path}}
    else
      _ ->
        Logger.error("Could not find file attachment in multipart body in incomming request")
        {:error, "file not found"}
    end
  end

  @spec construct_form_data(request_body(), file_key :: String.t(), path :: String.t()) ::
          list(tuple()) | list(none())
  defp construct_form_data(request_body, file_key, path) when request_body != %{} do
    # take the request body which is a map and convert it to a list of tuples {key, value}
    form_data_in_pairs =
      Map.delete(request_body, file_key)
      |> Enum.map(fn {form_data_key, form_data_value} ->
        {form_data_key, form_data_value}
      end)

    # and then add the path of the sent file (from incomming request) with it's key 
    # in 2 more tuples in order to create a list of tuples that the HTTPoison accepts
    # as multipart
    #
    # the filename should not have any valid name but it is required.  so just add the tmp name 
    # that the plug assigns to the file while it keeps it in memory
    base_data = [
      {:file, path, {"form-data", [{:name, file_key}, {:filename, Path.absname(path)}]}, []}
    ]

    Enum.concat(base_data, form_data_in_pairs)
  end

  defp construct_form_data(request_body, _, _) when request_body == %{}, do: []
end
