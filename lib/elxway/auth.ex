defmodule Elxway.Auth do
  @moduledoc """
  This module is used to determine if incoming requests are 
  authenticated or not
  """

  alias Elxway.ServiceRegistry.Manager

  @doc """
  Check if the path is a path where a token is needed
  """
  @spec path_info_is_whitelisted(path_info :: list(String.t())) :: boolean()
  def path_info_is_whitelisted(path_info) do
    case path_info do
      ["api", "login"] -> true
      _ -> false
    end
  end

  def is_authenticated?(auth_header) when auth_header == [], do: false

  @spec is_authenticated?(auth_header :: list(String.t())) :: boolean()
  def is_authenticated?(auth_header) do
    with {:ok, token} <- get_token_from_headers_if_exist(auth_header) do
      auth_host = Manager.get_host(:auth)
      url = "#{auth_host}/api/validate_token"

      headers = [{"Content-type", "application/json"}]
      body = Jason.encode!(%{"token" => token})
      response = HTTPoison.post!(url, body, headers) 

      Jason.decode!(response.body) |> token_result
    else
      {:error, _reason} -> false
    end
  end

  defp token_result(%{"valid" => true}), do: true
  defp token_result(%{"valid" => false}), do: false

  @spec get_token_from_headers_if_exist(auth_headers :: list(String.t())) ::
          {:ok, String.t()} | {:error, String.t()}
  defp get_token_from_headers_if_exist(auth_headers) do
    bearer_matcher = fn x -> String.starts_with?(x, "Bearer") end

    case Enum.find(auth_headers, bearer_matcher) do
      nil -> {:error, "token not found"}
      bearer_header -> String.split(bearer_header, " ") |> Enum.fetch(1)
    end
  end
end
