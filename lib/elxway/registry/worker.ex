defmodule Elxway.ServiceRegistry.Worker do
  use GenServer
  alias Elxway.ServiceRegistry.Manager

  @type available_worker_ids :: :worker_1 | :worker_2 | :worker_3 | :worker_4

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, opts)
  end

  @impl true
  def init(name: name) do
    Manager.register_worker(name)
    {:ok, %{}}
  end

  @spec update_paths_tree(worker_id :: available_worker_ids, tree :: map()) :: any()
  def update_paths_tree(worker_id, tree) do
    GenServer.cast(worker_id, {:update_paths_tree, tree})
  end

  @spec get_host(worker_id :: available_worker_ids, path_info :: list(String.t())) :: any()
  @doc """
  Based on a url (split into path parts), tries to find the host of the url.
  """
  def get_host(worker_id, path_info) do
    GenServer.call(worker_id, {:get_host, path_info})
  end

  @impl true
  def handle_call({:get_host, path_info}, _from, state) do
    path_items_count = Kernel.length(path_info)
    result = find(path_info, state, 1, path_items_count)
    {:reply, result, state}
  end

  @impl true
  def handle_cast({:update_paths_tree, new_tree}, current_tree) do
    new_state = Map.merge(current_tree, new_tree, &Elxway.ServiceRegistry.Registry.Utils.map_merger/3)
    {:noreply, new_state}
  end

  @spec find(list(String.t()), Struct, integer(), integer()) ::
          {:ok, String.t()} | {:error, String.t(), integer()}
  defp find(path_info, _, _, _) when path_info == [], do: {:error, "invalid_path", 400}
  defp find(_, paths_tree, _, _) when paths_tree == %{}, do: path_not_found()

  defp find(path_info, paths_tree, current_depth, total_depth) do
    [head | tail] = path_info

    subtree = get_subtree(paths_tree, head)

    if current_depth == total_depth do
      subtree |> select_host
    else
      subpaths = Map.get(subtree, :subpaths, %{})
      find(tail, subpaths, current_depth + 1, total_depth)
    end
  end

  defp get_subtree(paths_tree, current_path_item) do
    path_item_type = parse_path_item_type(current_path_item)

    if path_item_type == :int || path_item_type == :uuid do
      # in this case, we have an id, so we need to find
      # the matching placeholder in the map.
      # for example, if the path is api/entities/{entityId} we need to find the {entityId} key.
      entity_placeholder_key =
        Map.keys(paths_tree) |> Enum.find(fn x -> String.starts_with?(x, "{") end)

      Map.get(paths_tree, entity_placeholder_key, %{})
    else
      Map.get(paths_tree, current_path_item, %{})
    end
  end

  defp select_host(path_tree) do
    case Map.has_key?(path_tree, :host) do
      true -> {:ok, Map.get(path_tree, :host)}
      false -> path_not_found()
    end
  end

  defp parse_path_item_type(item) do
    with {_, _} <- Integer.parse(item) do
      :int
    else
      :error ->
        with {:ok, _} <- UUID.info(item) do
          :uuid
        else
          {:error, _} -> :string
        end
    end
  end

  defp path_not_found() do
    {:error, "not_found", 404}
  end
end
