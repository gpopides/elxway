defmodule Elxway.ServiceRegistry.WorkerSupervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    children = for worker_id <- 1..4  do
      name = name_to_atom("worker_#{worker_id}")
      Supervisor.child_spec({Elxway.ServiceRegistry.Worker, [name: name]}, id: worker_id)
    end
    Supervisor.init(children, strategy: :one_for_one)
  end

  @spec name_to_atom(name :: String.t()) :: :atom
  defp name_to_atom(name) do
    String.to_atom(name)
  end
end
