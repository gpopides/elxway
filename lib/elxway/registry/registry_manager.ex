defmodule Elxway.ServiceRegistry.Manager do
  use GenServer
  require Logger
  alias Elxway.ServiceRegistry.Worker
  alias Elxway.ServiceRegistry.Registry.Utils

  @type state() :: %{workers: map(), auth: String.t() | nil}

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    {:ok, %{workers: %{}, auth: nil}}
  end

  @spec register_worker(worker_id :: :atom) :: any()
  def register_worker(worker_id) do
    GenServer.cast(__MODULE__, {:register_worker, worker_id})
  end

  @type info :: %Elxway.Structs.RegisterInfo{}
  @spec register_endpoints(info) :: any()
  def register_endpoints(%Elxway.Structs.RegisterInfo{} = info) do
    GenServer.cast(__MODULE__, {:register_endpoints, info})
  end

  @type path_info :: list(String.t())
  @spec get_host(path_info | :auth) :: {:ok, String.t()} | {:error, String.t(), integer()}
  def get_host(:auth), do: GenServer.call(__MODULE__, :get_auth_host)

  def get_host(path_info) do
    select_worker() 
    |> increment_worker_counter
    |> Worker.get_host(path_info)
  end

  @spec unregister(worker_id :: atom) :: any()
  def unregister(worker_id) do
    GenServer.cast(__MODULE__, {:unregister_worker, worker_id})
  end

  defp select_worker() do
    GenServer.call(__MODULE__, :select_worker)
  end

  defp increment_worker_counter(worker_id) do
    GenServer.cast(__MODULE__, {:increment_worker_counter, worker_id})
    worker_id
  end

  @impl true
  def handle_call(:get_auth_host, _from, state) do
    {:reply, state.auth, state}
  end

  @impl true
  def handle_cast({:increment_worker_counter, worker_id}, state) do
    current_count = Map.get(state.workers, worker_id, 0)
    new_workers_map = %{state.workers | worker_id => current_count + 1}
    {:noreply, %{state | workers: new_workers_map}}
  end

  @impl true
  def handle_call(:select_worker, _from, state) do
    {worker_id_with_min_requests, _} = Enum.min_by(state.workers, fn {_worker_id, requests_count} -> requests_count end)
    {:reply, worker_id_with_min_requests, state}
  end

  @impl true
  def handle_cast({:unregister_worker, worker_id}, state) do
    new_workers_map = Map.delete(state.workers, worker_id)
    {:noreply, %{state | workers: new_workers_map}}
  end

  @impl true
  def handle_cast({:register_worker, worker_id}, state) do
    case Map.has_key?(state.workers, worker_id) do
      true ->
        {:noreply, state}

      false ->
        # init the worker with requests server counter = 0
        new_workers_map = Map.put_new(state.workers, worker_id, 0)
        new_state = %{state | workers: new_workers_map}

        {:noreply, new_state}
    end
  end

  @impl true
  def handle_cast({:register_endpoints, service_info}, state) do
    update_workers_tree(service_info, state.workers)
    Logger.info("Registered #{service_info.name}")

    # set the auth host if the service is the auth service
    if service_info.name == "authentication-service" do
      new_state = %{state | auth: service_info.host}
      {:noreply, new_state}
    else
      {:noreply, state}
    end
  end

  @spec update_workers_tree(service_info :: Elxway.Structs.RegisterInfo, workers :: list(:atom)) ::
          any()
  defp update_workers_tree(service_info, workers) do
    new_paths_tree = Utils.create_paths_tree(service_info)

    workers
    |> Enum.each(fn {worker_id, _}->
      Worker.update_paths_tree(worker_id, new_paths_tree)
    end)
  end
end
