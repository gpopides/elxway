defmodule Elxway.ServiceRegistry.Registry.Utils do
  def create_paths_tree(service_info) do
    service_info.paths
    |> Enum.map(fn path -> {service_info.host, path} end)
    |> Enum.reduce(%{}, fn {host, path}, acc ->
      tree = build_path_tree(host, String.split(path, "/"), %{})
      Map.merge(tree, acc, &map_merger/3)
    end)
  end

  defp build_path_tree(_, path_parts, tree) when path_parts == [], do: tree

  defp build_path_tree(host, path_parts, tree) do
    [head | tail] = path_parts
    subtree = build_path_tree(host, tail, tree)
    Map.put_new(tree, head, %{subpaths: subtree, host: host})
  end

  # @spec subpaths_merger(any(), tree1 :: state(), tree2 :: state()) :: %{subpaths: map(), host: String.t()}
  defp subpaths_merger(_, tree1, tree2) do
    merged_subpaths = Map.merge(tree1.subpaths, tree2.subpaths, &subpaths_merger/3)
    %{subpaths: merged_subpaths, host: tree1.host}
  end

  def map_merger(_, v1, v2) do
    updated_subpaths =
      Enum.reduce(v1.subpaths, v2.subpaths, fn {path_key, subpaths_of_path_key}, acc ->
        case Map.has_key?(acc, path_key) do
          true ->
            Map.update(acc, path_key, subpaths_of_path_key, fn x ->
              %{
                subpaths:
                  Map.merge(x.subpaths, subpaths_of_path_key.subpaths, &subpaths_merger/3),
                host: x.host
              }
            end)

          false ->
            Map.put_new(acc, path_key, subpaths_of_path_key)
        end
      end)

    %{
      subpaths: updated_subpaths,
      host: v1.host
    }
  end
end
