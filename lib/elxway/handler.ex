defmodule Elxway.Handler do
  import Plug.Conn
  alias Elxway.Structs.RegisterInfo
  alias Elxway.HandlerUtils
  alias Elxway.RequestForwarder
  alias Elxway.ServiceRegistry.Manager
  alias Elxway.Auth

  @type host :: String.t()
  @type conn :: %Plug.Conn{}
  @type path_info :: list(String.t())
  @type query_params :: map()

  def init(opts), do: opts

  def handle(conn) do
    if Auth.path_info_is_whitelisted(conn.path_info) do
      handle_request(conn)
    else
      if Auth.is_authenticated?(get_req_header(conn, "authorization")) do
        handle_request(conn)
      else
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(401, Jason.encode!(%{error: "invalid token"}))
      end
    end
  end

  defp handle_request(conn) do
    with {:ok, host} <- Manager.get_host(conn.path_info) do
      conn
      |> forward_request(host)
      |> return_response
    else
      {:error, reason, status_code} ->
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(status_code, Jason.encode!(%{error: reason}))
    end
  end

  @spec handle_register(conn) :: any()
  def handle_register(conn) do
    with {:ok, register_info} <- RegisterInfo.new(conn) do
      Manager.register_endpoints(register_info)

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Jason.encode!(%{registered: true}))
    else
      {:error} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(400, Jason.encode!(%{error: "invalid data", registered: false}))
    end
  end

  @spec forward_request(conn, host) :: any()
  defp forward_request(%{method: "GET"} = conn, host) do
    %Plug.Conn{path_info: path_info, query_params: query_params, req_headers: headers} = conn

    forwarded_response =
      HandlerUtils.get_forward_url(host, path_info, query_params) |> HTTPoison.get(headers)

    {conn, forwarded_response}
  end

  defp forward_request(%{method: "POST"} = conn, host) do
    %Plug.Conn{path_info: path_info, body_params: body, req_headers: headers} = conn
    forward_url = HandlerUtils.get_forward_url(host, path_info)
    content_type = get_req_header(conn, "content-type")

    case content_type do
      ["application/json"] ->
        {}
        forwarded_response = forward_url |> RequestForwarder.forward_post(body, headers, :json)
        {conn, forwarded_response}

      _ ->
        if HandlerUtils.is_multipart?(content_type) do
          response = forward_url |> RequestForwarder.forward_post(body, headers, :multipart)
          {conn, response}
        else
          {conn, {:error, "Content-Type #{Enum.join(content_type, " ")} not supported"}}
        end
    end
  end

  defp forward_request(%{method: "OPTIONS"} = conn, host) do
    %Plug.Conn{path_info: path_info} = conn
    response = HandlerUtils.get_forward_url(host, path_info) |> RequestForwarder.forward_options()
    {conn, response}
  end

  defp return_response({conn, {:ok, %{status_code: 404} = forwarded_response}}) do
    %{status_code: status, headers: _headers} = forwarded_response

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(
      status,
      Jason.encode!(%{error: "Route not found.  Check the route and the HTTP method"})
    )
  end

  defp return_response({conn, {:ok, forwarded_response}}) do
    %{status_code: status, body: body, headers: _headers} = forwarded_response

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status, body)
  end

  defp return_response({conn, {:error, reason}}) do
    with {:ok, encoded_response} <- Jason.encode(%{error: reason}) do
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(400, encoded_response)
    else
      _ ->
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(400, Jason.encode!(%{error: "something went wrong"}))
    end
  end
end
